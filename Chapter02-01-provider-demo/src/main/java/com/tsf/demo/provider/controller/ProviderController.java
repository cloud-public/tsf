package com.tsf.demo.provider.controller;

import com.tsf.demo.provider.ProviderApplication;
import com.tsf.demo.provider.config.FileReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.tsf.core.util.TsfSpringContextAware;
import org.springframework.web.bind.annotation.*;

import com.tsf.demo.provider.config.ProviderNameConfig;

import java.util.List;

@RestController
public class ProviderController {
    private static final Logger LOG = LoggerFactory.getLogger(ProviderController.class);

    @Autowired
    private ProviderNameConfig providerNameConfig;

    @RequestMapping(value = "/echo/{param}", method = RequestMethod.GET)
    public String echo(@PathVariable String param) {
        LOG.info("provider-demo -- request param: [" + param + "]");
        String result = "provider-demo-v1.0 request param: " + param + ", response from " + providerNameConfig.getName();
        LOG.info("provider-demo -- provider config name: [" + providerNameConfig.getName() + ']');
        LOG.info("provider-demo -- response info: [" + result + "]");
        return result;
    }

    @RequestMapping(value = "/config/{path}/value", method = RequestMethod.GET)
    public String config(@PathVariable String path) {
        return TsfSpringContextAware.getProperties(path);
    }

    @Value("${test.demo.prop:defalut}")
    private String propFromValue;

    @RequestMapping("/getValue")
    public String index() {
        String result = "propFromValue: " + propFromValue + "\n";
        return result;
    }

    @RequestMapping("/getFileConfiguration")
    public String getFileConfiguration(@RequestParam(required = true) String path) {
        List<String> file_str = FileReader.readTxtFileIntoStringArrList(path);
        StringBuffer content = new StringBuffer("");
        for (String str:file_str) {
            content.append(str + "\n");
        }
        return content.toString().replaceAll(" ","&nbsp;").replaceAll("\n","<br/>");
    }


}